/*jslint white: false, eqeqeq: false, plusplus: false, onevar: false, newcap: false */
/*global exports: false, require: false, console: false, log: false */
"use strict";
var Cc = require("chrome").Cc;
var Ci = require("chrome").Ci;
var testPageMod = require("pagemod-test-helpers").testPageMod;
var self = require("self");

var testURL = self.data.url('tests/blogPost.html');
var JSONifiedMessage = '{"cmd":"testMessage","data":{"a":"first","b":"second"}}';

exports['test SimplePageLoad'] = function (assert) {
	console.log("testURL = " + testURL);
	testPageMod(test, testURL, [{
		include: ["*"],
		contentScriptWhen: 'ready',
		contentScriptFile: [
			self.data.url("lib/getLinks.js")
		],
		onAttach: function onAttach(worker) {
			worker.on('message', function (msg) {
			  console.log("onAttach: msg = " + msg.toSource());
			  test.assertEqual(JSON.stringify(msg), "", "test of getting right links");
			  test.done();
			});
		}
	}],
	function (win, done) {
//		test.assertNotEqual(win.document.getElementsByTagName("form")[0],
//			null, "test of loading the page");
		done();
	});
};

//exports.ensurePageLoadsWell = function (test) {
//  var wm = Cc['@mozilla.org/appshell/window-mediator;1']
//           .getService(Ci.nsIWindowMediator);
//  var browserWindow = wm.getMostRecentWindow("navigator:browser");
//  if (!browserWindow) {
//    test.fail("page-mod tests: could not find the browser window, so " +
//              "will not run. Use -a firefox to run the pagemod tests.");
//    return null;
//  }

//  var loader = test.makeSandboxedLoader();
//  var pageMod = loader.require("page-mod");
//  var testDoc = {}, b = {}, tabBrowser = {}, newTab  = {};

//  pageMod.PageMod({
//		include: ["*"],
//		contentScriptWhen: 'ready',
//		contentScriptFile: [
//			self.data.url("libPW.js"),
//			self.data.url("pageWorker.js")
//		],
//		onAttach: function onAttach(worker) {
//			worker.on('message', function (msg) {
//				switch (msg.cmd) {
//					case "testReady":
//						testDoc = b.contentWindow.wrappedJSObject.document;
//						test.assertNotEqual(testDoc.getElementById("dupeid_action"),
//							null, "test of DOM modifications of the page");
//						pageMod.destroy();
//						tabBrowser.removeTab(newTab);
//						test.done();
//						// the test itself
//						break;
//					default:
//						main.messageHandler(worker, msg);
//				}
//			});
//		}
//	});

//  tabBrowser = browserWindow.gBrowser;
//  newTab = tabBrowser.addTab(testURL);
//  tabBrowser.selectedTab = newTab;
//  b = tabBrowser.getBrowserForTab(newTab);
//};
