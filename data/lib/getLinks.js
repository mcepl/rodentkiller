function collectLinks() {
  var interestingRelations = ['prev', 'next'];
  var links = {};

  var linkElements = document.getElementsByTagName("link");
  var ourLinkElems = Array.filter(linkElements, function(elem) {
    if (elem.hasAttribute("rel")) {
      var rel = elem.getAttribute("rel").toLowerCase();
      return (interestingRelations.indexOf(rel) !== -1);
    }
    return false;
  });

  ourLinkElems.forEach(function (elem) {
    var key = elem.getAttribute('rel').toLowerCase();
    // don't use .getAttribute("href") here because .href gives an absolute URL
    links[key] = elem.href;
  });

  self.postMessage(links);
}

collectLinks();
