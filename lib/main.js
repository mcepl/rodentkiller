// Released under the MIT/X11 license
// http://www.opensource.org/licenses/mit-license.php
//
"use strict";
var Hotkey = require("sdk/hotkeys").Hotkey;
var tabs = require("sdk/tabs");
var pageWorkers = require("sdk/page-worker");
var self = require("sdk/self");

function collectLinks(cb) {
  pageWorkers.Page({
    contentURL: tabs.activeTab.url,
    contentScriptFile: self.data.url("lib/getLinks.js"),
    contentScriptWhen: "ready",
    onMessage: function(msg) {
      cb(msg);
    }
  });
}

function goUp() {
    var curURL = tabs.activeTab.url.replace(/\/?$/,'');
    var curURLArr = curURL.split("/");
    if (curURLArr.length > 3) {
      curURLArr = curURLArr.slice(0,-1);
    }
    var newURL = curURLArr.join("/");
    if (newURL !== curURL) {
      tabs.activeTab.url = newURL;
    }
}

function goRel(newRel) {
  collectLinks(function (links) {
    if (links.hasOwnProperty(newRel)) {
      tabs.activeTab.url = links[newRel];
    }
  });
}

function goNext() {
  goRel("next");
}

function goPrevious() {
  goRel("prev");
}

var upHotKey = Hotkey({
  combo: "alt-up",
  onPress: function() {
    goUp();
  }
});

var nextHotKey = Hotkey({
  combo: "alt-pagedown",
  onPress: function() {
    goNext();
  }
});

var prevHotKey = Hotkey({
  combo: "alt-pageup",
  onPress: function() {
    goPrevious();
  }
});
